const ObjectId = require('mongodb').ObjectID;
const mongo = require('../../db.js');
const Random = require('@supercharge/strings')

mongo.connectToServer( function( err) {  
    if (err) console.log(err)
    db = require('../../db.js').getDb()
    medicionesCollection = db.collection('mediciones')
})

function newMedicion(medicion){ return new Promise((resolve, reject) => {
    medicion.created_ts = Date.now()
    medicionesCollection.insertOne(medicion).then( () => {   
        resolve ({'result':'success' })  
    }).catch(error => console.error(error))  
})}

function get_home(body){ return new Promise((resolve, reject) => {
    console.log(body)
    medicionesCollection.find({ created_ts:{ $lt:body.to, $gte:body.since }}).toArray(function (err, docs) {
        if (docs && docs.length) {
            resolve ({ 
                result:'success', 
                data:docs
            })
        }
        else{
            resolve ({ result: 'fallo'})
        }
    })
})}

module.exports ={ newMedicion, get_home}

