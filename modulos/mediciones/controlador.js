const servicio = require('./servicio')
const {logger} = require('../../logger')

async function create(req, res){
    const medicion = req.body

    logger.info({ action: 'request to create new medicion' })

    const {result} = await servicio.newMedicion(medicion)

    if(result==='success'){logger.info({ action: 'new medicion created' })}
    else{logger.warn({ action: 'new medicion creation fail' })}

    res.writeHead(200, 'Content-Type', 'application/json');
    res.end(JSON.stringify({ result: result }));
}

async function getHome(req, res){

    logger.info({ action: 'request home'})

    const {result,data} = await servicio.get_home(req.body)

    if(result==='success'){logger.info({ action: 'home sent succesfully'})}
    else{logger.warn({ action: 'home not send' })}
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify({ result: result, data:data }))
}

module.exports ={ create, getHome}