const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const mediciones = require('./modulos/mediciones/controlador')

const port = process.env.MEDICIONES_PORT

process.on('uncaughtException', err => {
  logger.error({action: 'Fatal error', data:{error:err}});
  setTimeout(() => { process.exit(0) }, 1000).unref() 
})

process.on('unhandledRejection', (err, err2) => {
  logger.error({action: 'Fatal error', data:{error:err}});
  setTimeout(() => { process.exit(0) }, 1000).unref() 
})

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.post('/create', (req, res) => { mediciones.create(req, res)})
app.post('/get_home', (req, res) => { mediciones.getHome(req, res)})

app.get('/', (req, res) => {res.writeHead(200, 'Content-Type', 'application/json'); res.end('Servicio corriendo ok');})

app.listen(port, function () { logger.info({ action: 'Service running on port ' + port}) } )








