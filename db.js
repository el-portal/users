const MongoClient = require("mongodb").MongoClient;

const urlMongo = 'mongodb://'+ process.env.MONGO_HOSTNAME + ':' + process.env.MONGO_PORT
const database = process.env.DATABASE_NAME
var db;

function connectToServer( callback ) {
    MongoClient.connect(urlMongo,  { useUnifiedTopology: true , useNewUrlParser: true }, function( err, client ) {
        db  = client.db(database);
        return callback( err );
    })
}

function getDb() {
    return db
}

module.exports = {connectToServer, getDb}